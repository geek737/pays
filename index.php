<?php
$chemin="pays.txt";
$fpays=file($chemin);// sera recuperer comme array
usort($fpays,function($pays1,$pays2){
    return $pays1<=>$pays2;
});

function getPaysWithFirstLetter($letter){
    $list=array();
    foreach($GLOBALS["fpays"] as $line){
        if(strtolower(substr($line,0,1))==strtolower($letter))
            array_push($list,$line);
    }
    return $list;
}
// getPaysWithFirstLetter("a");
$listLetter=array();
foreach (range('A', 'Z') as $char) {
    array_push($listLetter, $char);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Pays</title>
</head>
<body>
    <div class="menu">
    <?php

        foreach($GLOBALS["listLetter"] as $line)
        {
            echo "<a href=#$line>$line</a>";
        }?>
    </div>
    <div class="container">
            
    <?php
        foreach($GLOBALS["listLetter"] as $line)
        {
            $list=getPaysWithFirstLetter($line);
            echo "<div class='containerPays' id=".$line."><h2>".$line."</h2>
            <ul>";
            foreach($list as $line){
                echo "<li><a>$line</a></li>";
            }
            echo"</ul></div>";
        }
    ?>
    </div>
</body>
</html>